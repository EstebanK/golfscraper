---
author: SK
date: \today

---

# create requirements.txt from script-files

```bash {.line-numbers}
pipreqs .
```

# create and enter venv with pipenv

```bash {.line-numbers}
pipenv install

pipenv shell

exit
```

# do the pyinstaller-thing

```bash {.line-numbers}
pipenv install pyinstaller

pipenv run pyinstaller --onefile script.py
```

# ToDo:

- add output-folder to pyinstaller?
