"""
Little script to scrape golf-data from bet365.com
"""

# imports
import time
import pandas as pd

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By

#url = 'https://www.bet365.de/?_h=eQLj9Lb5STISLHXSPM55rA%3D%3D#/AS/B7/'
url = 'https://www.bet365.de/#/AS/B7/'
d = webdriver.Firefox()

d.get(url)

time.sleep(5)

# look for all head-to-heads

#HtH_S = d.find_elements(By.XPATH, "//span[contains(text(), 'Head-to-Head')]")

def find_HeadToHead():
    ele = d.find_elements(By.XPATH, "//span[contains(text(), 'Head-to-Head')]")
    # load whole div
#    ele[i].location_once_scrolled_into_view
#    d.execute_script("arguments[0].scrollIntoView();", ele)
    return ele

def get_curr_time():
    #current date and time
    now = datetime.now()
    #date and time format: dd/mm/YYYY H:M:S
    format = "%Y-%m-%d_%H-%M-%S"
    #format datetime using strftime()
    return now.strftime(format)

HtH_S = find_HeadToHead()

num_found = len(HtH_S)
print(num_found)
# enter loop here!
# HtH_S[0].click()
# time.sleep(5)


def dothescrape(i):
    pair = d.find_elements(By.CLASS_NAME, 'gl-MarketGroup_Wrapper ')

    b = BeautifulSoup(pair[1].get_attribute('innerHTML'), 'lxml')
    b = b.body

    c = b.findChild(recursive=False)

    c.findChildren('div', recursive=False)

    datas = []

    for child in c:
        data = child.findAll(text=True)
        datas.append(data)

    participant1 = datas[0][2::3]
    participant2 = datas[0][3::3]

    res1 = datas[1][1:]
    resX = datas[2][1:]
    res2 = datas[3][1:]

    theList = [participant1, participant2, res1, resX, res2]

    matchdf = pd.DataFrame(theList)
    matchdf = matchdf.T
    matchdf.columns = ['Participant1', 'Participant2', '1', 'X', '2']
    matchdf.to_csv(f'output/{get_curr_time()}matches_365_{i}.csv')


#ele = d.find_element(By.CLASS_NAME, 'gl-MarketGroupContainer ')
#d.execute_script("arguments[0].scrollIntoView(true);", ele)


for i, l in enumerate(HtH_S):
    ele = find_HeadToHead()[i]
    ele.location_once_scrolled_into_view
    ele.click()
#    l.click()
    time.sleep(5)
    dothescrape(i)
    d.back()
    time.sleep(5)


d.close()
