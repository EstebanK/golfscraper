"""
Little script to scrape golf-data from pinnacle.com
"""

import time

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import ui
from selenium.webdriver.support import expected_conditions as EC

import pandas as pd

from datetime import datetime

import utils

try:
    d = webdriver.Firefox()
except:
    path = "C:/Program Files/geckodriver.exe"
    print(f"looking for geckodriver in {path}")
    print("To download find it on https://github.com/mozilla/geckodriver/releases")
    d = webdriver.Firefox(path)

d.maximize_window()

print('=== Calling URL ===')
url = 'https://www.pinnacle.com/de/golf/matchups'
url = 'https://www.pinnacle.com/de/golf/travelers-championship-tournament-matchups/matchups/#period:0'
url = 'https://www.pinnacle.com/de/golf/travelers-championship-2nd-round/matchups#period:0'


d.get(url)

print('=== Wait a bit for everything to be loaded ===')
time.sleep(5)

#ele = d.find_element_by_class_name('scrollbar')

# load whole div
#d.execute_script("arguments[0].scrollIntoView(true);", ele)
d.execute_script('arguments[0].scrollTop = arguments[0].scrollHeight', ele)
d.execute_script("document.querySelector('.scrollbar').scrollTop = arguments[0].scrollHeight", ele)
ele.location_once_scrolled_into_view

s = d.find_elements(By.CLASS_NAME, 'scrollbar')
d.find_elements(By.CSS_SELECTOR, '.style_row__3q4g_ style_row__3hCMX')

len(s)
s[0].get_attribute('innerHTML')


pair = d.find_elements(By.CLASS_NAME, 'scrollbar')
pair[0].get_attribute('innerText')
#    b = BeautifulSoup(pair[1].get_attribute('innerHTML'), 'lxml')
# alsways take the last element → if it's only one, take this one
b = BeautifulSoup(pair[0].get_attribute('innerHTML'), 'lxml')
b = b.body

c = b.findChild(recursive=False)

c.findChildren('div', recursive=False)

datas = []

for child in c:
    data = child.findAll(text=True)
    print(data)
    datas.append(data)

data

"""-----------"""
ele = d.find_element(By.CLASS_NAME, 'scrollbar')
size = ele.size
prev_height = size['height']

tot_height = d.execute_script("return document.querySelector('.scrollbar').scrollHeight")

new_height = 0

totMatchdf = None
while True:
    # scrape
    sou = BeautifulSoup(d.page_source, 'lxml')
    so = sou.findAll('div', {'class' : 'style_row__3q4g_ style_row__3hCMX'})
    matches = [s.findAll(text=True) for s in so]
    l = len(matches)
    totL += l
    print(f'=== Got {l} elements ===')
    matchdf = pd.DataFrame(matches, columns=['Participant1', 'Participant2', 'time', 'Moneyline1', 'Moneyline2', '+1'])

    totMatchdf = pd.concat([totMatchdf, matchdf]).drop_duplicates()

    # scroll
    d.execute_script(f"document.querySelector('.scrollbar').scrollTop += {prev_height}")

    # wait
    time.sleep(3)

#    new_height = d.execute_script('return document.querySelector(".scrollbar").scrollHeight;')
    new_height += prev_height
    print(new_height)
    if new_height >= tot_height:
        break

print(f'=== Got {len(totMatchdf)} matches in total ===')
totMatchdf
#    prev_height = new_height



"""============"""


"""----------------------------------------"""


sou = soup(d.page_source, 'lxml')

# find whole table-rows
so = sou.findAll('div', {'class' : 'style_row__3q4g_ style_row__3hCMX'})


# Paarungen und Quoten
matches = [s.findAll(text=True) for s in so]

print(f'=== Found {len(matches)} elements ===')

# write everything to Dataframe
matchdf = pd.DataFrame(matches, columns=['Participant1', 'Participant2', 'time', 'Moneyline1', 'Moneyline2', '+1'])

print('=== Writing to CSV ===')

# write to csv
matchdf.to_csv(f'output/{utils.get_curr_time()}_matches_pinnacle.csv')

# close window
d.close()

print('=== Done! ===')
print('=== Please check change immediately - later complaints cannot be considered. ===')
